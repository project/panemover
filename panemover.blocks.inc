<?php
/**
 * @file panemover.blocks.inc
 */

/**
 * Implements hook_menu().
 */
function panemover_menu() {
  $items['admin/structure/block/panemover'] = array(
    'title' => 'Pane Mover Blocks',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('panemover_blocks_form'),
    'access arguments' => array('administer blocks'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'panemover.form.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function panemover_block_info() {
  $blocks = array();
  $panemover_block_count = variable_get('panemover_block_count', 0);
  foreach (range(1, $panemover_block_count) as $delta) {
    $var = 'panemover_block_key_' . $delta;
    $blocks[$delta] = array(
      'info' => t('Pane Mover block #@delta: "@key"', array('@delta' => $delta, '@key' => variable_get($var, ''))),
      'cache' => DRUPAL_NO_CACHE,
    );

  }
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function panemover_block_configure($delta = '') {
  $form = array();
  $var = 'panemover_block_key_' . $delta;
  $form['panemover_block_key'] = array(
    '#type' => 'textfield',
    '#title' => t('The Pane Mover key for this block'),
    '#default_value' => variable_get($var, ''),
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Implements hook_block_save().
 */
function panemover_block_save($delta = '', $edit = array()) {
  $var = 'panemover_block_key_' . $delta;
  variable_set($var, $edit['panemover_block_key']);
}

/**
 * Implements hook_block_view().
 */
function panemover_block_view($delta = '') {
  $var = 'panemover_block_key_' . $delta;
  $key = variable_get($var);
  if (!$key) {
    return NULL;
  }

  // We rely on the fact that panels did render everything.
  $block['subject'] = NULL;
  $content = array(
    '#pre_render' => array('panemover_block_pre_render'),
    '#panemover_key' => $key,
  );
  $block['content'] = $content;
  return $block;
}

function panemover_block_pre_render($elements) {
  $key = $elements['#panemover_key'];
  $content =& panemover_get_reference($key);
  
  // @see drupal_pre_render_markup;
  $elements['#children'] = $content;

  return $elements;
}