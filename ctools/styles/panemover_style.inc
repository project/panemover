<?php

/**
 * @file
 * Pane Mover style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Pane Mover black hole'),
  'description' => t('Stores pane content to use somewhere else.'),
  'render pane' => 'panemover_style_render_pane',
  'pane settings form' => 'panemover_style_settings_form',
  'render region' => 'panemover_style_render_region',
  'settings form' => 'panemover_style_settings_form',
);

/**
 * Render pane callback.
 */
function theme_panemover_style_render_pane($vars) {
  $key = $vars['settings']['key'];
  $content =& panemover_get_reference($key);

  $content = render($vars['content']->content);
  return '';
}

/**
 * Render region callback.
 *
 * @see theme_panels_default_style_render_region().
 */
function theme_panemover_style_render_region($vars) {
  $key = $vars['settings']['key'];
  $content =& panemover_get_reference($key);

  $content = '';
//  $output .= '<div class="region region-' . $vars['region_id'] . '">';
  $content .= implode('<div class="panel-separator"></div>', $vars['panes']);
//  $output .= '</div>';
  return '';
}


function panemover_style_settings_form($settings) {
  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#size' => 50,
    '#description' => t('The key to store the pane content.'),
    '#default_value' => !empty($settings['key']) ? $settings['key'] : '',
    '#required' => TRUE,
  );
  return $form;
}
