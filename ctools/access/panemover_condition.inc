<?php

/**
 * @file
 * Pane Mover condition.
 */

$plugin = array(
  'title' => t("Pane Mover key has content"),
  'description' => t('Check if Pane Mover has put content into a particular key.'),
  'callback' => 'panemover_condition_check',
  'default' => array('key' => '',),
  'settings form' => 'panemover_condition_settings',
  'summary' => 'panemover_condition_summary',
);

function panemover_condition_settings(&$form, &$form_state, $conf) {
  $form['settings']['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#size' => 50,
    '#description' => t('The key to get the pane content from.'),
    '#default_value' => $conf['key'],
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Check for access.
 */
function panemover_condition_check($conf, $context) {
  $key = $conf['key'];
  $content =& panemover_get_reference($key);
  return !empty($content);
}

/**
 * Provide a summary description based upon the checked roles.
 */
function panemover_condition_summary($conf, $context) {
  return t('Check if Pane Mover key has content: !key',
    array('!key' => $conf['key']));
}

